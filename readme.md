# kawaScript2 for Reaper v5.50~.

* Extension For Cockos REAPER v5.50~.
* Scripts info and Animation Preview at [Wiki Page](http://kawa.works/reascript-midi-section).
* Some actions need ["SWS / S&M Extension"](http://www.sws-extension.org/).
* Thank you Reaper And [ReaPack](https://reapack.com/) And Sws extensiton!

# Animation Preview and Documents.

1.  [MIDI-Section](http://kawa.works/reascript-midi-section)
2.  [MIDI-CC-Section](http://kawa.works/reascript-midi-cc-section)
3.  [MAIN-Section](http://kawa.works/reascript-main-section)
4.  [Automaion Items-Section](http://kawa.works/reascript-automation-items)
5.  [Change log](http://kawa.works/reascript-change-log)

# Other Projects ( Plugins )

* kawaChord2 [KVR Audio Product Page](http://www.kvraudio.com/product/kawachord2-by-kawa/details).
* kawaChord2 Free [KVR Audio Product Page](https://www.kvraudio.com/product/kawachord2-free-by-kawa).
